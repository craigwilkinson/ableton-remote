from sys import argv 
import SocketServer

DEFAULT_HOST = "192.168.1.70"
DEFAULT_PORT = 1235

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        while True:
            # self.request is the TCP socket connected to the client
            self.data = self.request.recv(1024)
            datas = self.data.split(",")
            print datas[0] + " said " + datas[1]
            # just send back the same data, but upper-cased
            # self.request.sendall(self.data.upper())

if __name__ == "__main__":
    if len (argv) == 3:
        HOST, PORT = argv[1], int(argv[2])

    else:
        HOST, PORT = DEFAULT_HOST, DEFAULT_PORT
        
    # start server on local host
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
    
