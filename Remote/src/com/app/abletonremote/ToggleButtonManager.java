package com.app.abletonremote;

import java.lang.IllegalArgumentException;
import java.util.List;
import java.util.ArrayList;
import android.view.View;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.graphics.Typeface;

/** This is used instead of a radio button list because
 * trying to get nice themes to work with radio buttons was a nightmare.
 * @author Craig Wilkinson
 */
public class ToggleButtonManager {

    private List<ToggleButton> buttons;
    private int toggledId;
    private EQActivity parentActivity;
    private View parentView;

    private int buttonsLoaded;

    
    public ToggleButtonManager() {
        this.buttons = new ArrayList();

        toggledId = -1;
    }

    public void setGUI(EQActivity parent) {
        this.parentActivity = parent;
    }

    public void addButton(ToggleButton button) {
        buttons.add(button);
    }

    public void toggle(int id) {
        if (id < buttons.size() && id >= 0) {
            toggledId = id;

            // we only want to toggle the button if it isn't already
            if (!buttons.get(id).isToggled())
                buttons.get(id).toggle();

            // untoggle all the other buttons
            for (int i = 0; i < buttons.size(); i++) {
                if (i != id)
                    buttons.get(i).setToggle(false);
            }
        } else {
            throw new IllegalArgumentException("invalid id passed: " + id);
        }
    }

    public int getToggledId() {
        return toggledId;
    }

}
