package com.app.abletonremote;

import android.widget.Button;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.drawable.GradientDrawable;

public class ToggleButton extends Button {
    private final int TOGGLED_BUTTON_COLOR = 0xffdddddd;
    private final int UNTOGGLED_BUTTON_COLOR = 0xffbdbdbd;

    private boolean toggled;
    private int id;

    public ToggleButton(Context context) {
        super(context);

        toggled = false;
    }

    public ToggleButton(Context context, AttributeSet attr) {
        super(context, attr);

        toggled = false;
    }

    public ToggleButton(Context context, AttributeSet attr, int defst) {
        super(context, attr, defst);

        toggled = false;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void toggle() {
        toggled = !toggled;

        // change the background color
        GradientDrawable drawable = (GradientDrawable) getBackground();
        if (toggled)
            drawable.setColor(TOGGLED_BUTTON_COLOR);
        else
            drawable.setColor(UNTOGGLED_BUTTON_COLOR);

    }

    public void setToggle(boolean toggle) {
        this.toggled = toggle;

        GradientDrawable drawable = (GradientDrawable) getBackground();
        if (toggled)
            drawable.setColor(TOGGLED_BUTTON_COLOR);
        else
            drawable.setColor(UNTOGGLED_BUTTON_COLOR);
    }

    public boolean isToggled() {
        return toggled;
    }
}
