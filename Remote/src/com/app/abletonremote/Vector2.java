package com.app.abletonremote;

/** A class to represent a point on the screen.
 */
public class Vector2 {
    public double X;
    public double Y;

    public Vector2 (double x, double y) {
        this.X = x;
        this.Y = y;
    }

    @Override
    public String toString() {
        return ("(" + this.X + "," + this.Y + ")");
    }
}
