package com.app.abletonremote;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/** This activity hosts the EQ view. It ties the GUI elements together,
 * initializes everything and serves as a listener for system events such as
 * back button presses.
 * @author Craig Wilkinson
 */
public class EQActivity extends Activity
{
    private final int CHANNEL_COUNT = 8;

    private final int CHANNEL_BUTTON_BACKGROUND_UNSET = 0xFFBDBDBD;
    private final int CHANNEL_BUTTON_BACKGROUND_PRESSED = 0xFFEEEEEE;
    private final int CHANNEL_BUTTON_BACKGROUND_SET = 0xFFDDDDDD;
    private final int CHANNEL_BUTTON_TEXT_COLOR = 0xFFFFFFFF;

    private FrequencyView frequencyView;
    private int currentChannel;

    NetworkManager network;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // set up networking
        network = new NetworkManager();

        // construct the frequencyview and add it to the layout
        frequencyView = new FrequencyView(this, this);
        frequencyView.setNetworkManager(network);
        final FrameLayout frequency = (FrameLayout) findViewById(R.id.freq_part);
        frequency.addView(frequencyView);

        // we have to programatically match the
        // width of the buttons to the height (which are
        // the height of their parent)
        final LinearLayout buttonsContainer = (LinearLayout) findViewById(R.id.butcontainer);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/FuturaLight.ttf");

        // load all of the channel select buttons
        for (int i = 0; i < CHANNEL_COUNT; i++) {
            final Button b = new Button(this);
            final int id = i;

            // set the button styles
            b.setTypeface(tf);
            b.setTextColor(CHANNEL_BUTTON_TEXT_COLOR);
            b.setTextSize(25);
            b.setText(String.valueOf(i + 1));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            lp.setMargins(0, 0, 10, 0); // set a margin on the right for each button
            b.setLayoutParams(lp);
            b.setWidth(0);//(int) Math.round(buttonsContainer.getWidth() / (double) CHANNEL_COUNT + 4));
            b.setHeight(0);//(int) Math.round(buttonsContainer.getHeight() / 2.0));
            b.setBackgroundResource(R.drawable.buttoncircle);

            // clicking this button sets the current channel to whichever button this is
            b.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent e) {
                    final int actionType = e.getAction();
                    final GradientDrawable buttonBackground = (GradientDrawable) b.getBackground();

                    if (actionType == MotionEvent.ACTION_DOWN) {
                        buttonBackground.setColor(CHANNEL_BUTTON_BACKGROUND_PRESSED);

                        // unset the colour of the previously selected channel
                        Button oldButton = (Button) buttonsContainer.getChildAt(currentChannel);
                        GradientDrawable oldBackground = (GradientDrawable) oldButton.getBackground();
                        oldBackground.setColor(CHANNEL_BUTTON_BACKGROUND_UNSET);

                        currentChannel = id;

                    } else if (actionType == MotionEvent.ACTION_UP) {

                        // this is now the set one if there can be an action up
                        buttonBackground.setColor(CHANNEL_BUTTON_BACKGROUND_SET);
                    }

                    return true;
                }
            });

            buttonsContainer.addView(b);
        }

        // we can't actually go ahead if these are null. This should never happen
        assert (buttonsContainer != null && buttonsContainer.getViewTreeObserver() != null);

        // fix all the button sizes once the layout tree is finished
        // this must be done because the button container initially
        // has a size of zero.
        buttonsContainer.getViewTreeObserver().addOnGlobalLayoutListener(
            new OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    for (int i = 0; i < CHANNEL_COUNT; i++) {
                        Button b = (Button) buttonsContainer.getChildAt(i);

                        // all the buttons should be there at this point
                        assert (b != null);

                        b.setWidth(buttonsContainer.getHeight() - 30);
                        b.setHeight(buttonsContainer.getHeight() - 30);
                    }
                }
            }
        );

        currentChannel = 0;

        // now the same for the buttons on the right
        Button freqButton = (Button) findViewById(R.id.freqButton);
        freqButton.setTypeface(tf);
        freqButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xffffffff);
                } else if (e.getAction() == MotionEvent.ACTION_UP
                        || e.getAction() == MotionEvent.ACTION_CANCEL) {

                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xff787878);
                }

                return true;
            }
        });

        Button gainButton = (Button) findViewById(R.id.gainButton);
        gainButton.setTypeface(tf);
        gainButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xffffffff);
                } else if (e.getAction() == MotionEvent.ACTION_UP
                        || e.getAction() == MotionEvent.ACTION_CANCEL) {

                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xff787878);
                }

                return true;
            }
        });

        Button qButton = (Button) findViewById(R.id.qButton);
        qButton.setTypeface(tf);
        qButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xffffffff);
                } else if (e.getAction() == MotionEvent.ACTION_UP
                        || e.getAction() == MotionEvent.ACTION_CANCEL) {

                    GradientDrawable drawable = (GradientDrawable) v.getBackground();
                    drawable.setColor(0xff787878);
                }

                return true;
            }
        });

    }

    public int getCurrentChannel() {
        return currentChannel;
    }

    @Override
    public void onDestroy() {
        network.closeConnection();
        super.onDestroy();
    }

}
