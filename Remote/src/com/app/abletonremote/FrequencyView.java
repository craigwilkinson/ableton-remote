package com.app.abletonremote;

import java.util.List;
import java.util.ArrayList;

import android.graphics.PixelFormat;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.MotionEvent;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Rect;
import android.graphics.DashPathEffect;
import android.content.Context;
import android.util.AttributeSet;

public class FrequencyView extends SurfaceView
                           implements SurfaceHolder.Callback {

    private final int APP_BACKGROUND_COLOR= 0xff787878;
    private final int GRAPH_BACKGROUND_COLOR = 0xffffffff;
    private final int VERT_LINE_COLOR = 0xffe1e1e1;
    private final int HORIZ_LINE_COLOR = 0xff82e0e2;
    private final int LINE_COLOR = 0xff82e0e2;

    private final int TEXT_COLOR = 0xffaaaaaa;
    private final int TEXT_BACKGROUND_COLOR = 0xffffffff;
    private final int TEXT_SIZE = 40;
    private final int TEXT_BOX_BUFFER = 2;
    private final int TEXT_BOTTOM_MARGIN = 10;
    private final int TEXT_XPOS_SUBTRACTION = 10;

    private final int Y_SCALE_AMPLITUDE = 12;
    private final int Y_SCALE_MARGIN = 10;
    private final int Y_SCALE_TEXT_SHIFT = 5;

    private final int ROUNDING_RADIUS = 10;
    private final int VERT_MARGIN = 20;
    private final int LEFT_MARGIN = 40; // to make room for the text
    private final int NUM_ROWS = 5;

    private final int CHANNEL_COUNT = 8;
    private final int INITIAL_STANDARD_DEVIATION = 30;
    private final double GAUSSIAN_SCALAR = 1; // because gaussians have small values

    // for the log scale
    private final int FREQ_END = 25000;

    private int yScale; // from 0 position to top
    private int xScale; // from left to right
    // the values of each filter position

    private List<AmplifiedGaussian> gaussians;

    private Paint vertGridPaint;
    private Paint horizGridPaint;
    private Paint linePaint;
    private Paint backgroundPaint;
    private Paint scaleTextPaint;
    private Paint scaleTextRectPaint;

    private NetworkManager nm;

    private EQActivity parent;

    private SurfaceHolder holder;

    // keep track of when we are adjusting variance so that when fingers are released we don't
    // set the mean afterwards
    private boolean adjustingVariance;

    /** A class to use the shape of a normal distribution,
     * but can be amplified (therefore does not abide by all
     * rules of a normal distribution; the integral of the distribution
     * is NOT one).
     * Having said that, this class still behaves just like a normal
     * distribution, just with the getValue method which returns the
     * amplified density at a particular x value (and the getters and setters
     * for the amplitude field). Note that not all constructors are
     * implemented (through laziness/I don't need them here).
     * @author Craig Wilkinson
     */
    private class AmplifiedGaussian {
        double amplitude;
        double mean;
        double sd;

        public AmplifiedGaussian(double mean, double sd) {
            this.mean = mean;
            this.sd = sd;

            this.amplitude = 0.0;
        }

        public AmplifiedGaussian(double mean, double sd,
                                           double amplitude) {
            this(mean, sd);

            this.amplitude = amplitude;
        }

        public double getAmplitude() {
            return amplitude;
        }

        public void setAmplitude(double amplitude) {
            this.amplitude = amplitude;
        }

        public double getMean() { return mean; }

        public void setMean(double mean) { this.mean = mean; }

        /* Get the amplified density at the given x value */
        public double getValue(double x) {
            // we don't actually use the standard deviation in the
            // normalization because we don't want the height to
            // change when the width changes
            double normalize = 1.0 / (Math.sqrt(2 * Math.PI));

            double exponent = -1.0 * (Math.pow(x - mean, 2.0)) /
                                     (2.0 * Math.pow(sd, 2.0));

            double normal = normalize * (Math.pow(Math.E, exponent));

            return amplitude * normal;
        }

        // set the standard deviation
        public void setSD(double sd) {
            this.sd = sd;
        }
    }


    public FrequencyView(Context context, EQActivity parent) {
        super(context);

        this.parent = parent;

        // standard surfaceview setup
        holder = getHolder();
        holder.addCallback(this);
        holder.setFormat(PixelFormat.TRANSLUCENT);

        // set up paints
        // set the grid color
        vertGridPaint = new Paint();
        vertGridPaint.setColor(VERT_LINE_COLOR);
        vertGridPaint.setStrokeWidth(0);

        // dashed, blue lines for the horizontal ones
        horizGridPaint = new Paint();
        horizGridPaint.setColor(HORIZ_LINE_COLOR);
        horizGridPaint.setStrokeWidth(2);
        float[] intervals = {1, 3};
        horizGridPaint.setPathEffect(new DashPathEffect(intervals, 0));

        // paint for drawing the rounded background
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(GRAPH_BACKGROUND_COLOR);

        // paint for drawing the frequency line itself
        linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        linePaint.setStrokeWidth(5);
        linePaint.setColor(LINE_COLOR);
        linePaint.setStrokeJoin(Paint.Join.ROUND);

        // fonts for the scale
        scaleTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/FuturaLight.ttf");
        scaleTextPaint.setTypeface(font);
        scaleTextPaint.setColor(TEXT_COLOR);
        scaleTextPaint.setTextSize(TEXT_SIZE);
        scaleTextPaint.setSubpixelText(true);

        // font background
        scaleTextRectPaint = new Paint();
        scaleTextRectPaint.setColor(TEXT_BACKGROUND_COLOR);

        adjustingVariance = false;
    }

    // allows this class to send messages to the network
    public void setNetworkManager(NetworkManager nm) {
        this.nm = nm;
    }

    // get the touches. If just one touch point is made, adjust the amplitude of the
    // selected channel. If two touch points are made, adjust the standard deviation
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN
         || event.getAction() == MotionEvent.ACTION_MOVE) {

            int channel = parent.getCurrentChannel();
            AmplifiedGaussian selectedGaussian = gaussians.get(channel);

            double amplitude;
            double mean;

            // if there are two touches, also set the standard deviation
            // of the gaussian
            if (event.getPointerCount() == 2) {
                adjustingVariance = true;

                // get the mean and the amplitude from the average of the touches
                Vector2 averagePoint = getMeanPointFromTouches(event);
                mean = averagePoint.X;
                amplitude = getAmplitudeFromY(averagePoint.Y);

                // set the standard deviation to the separation
                double deviation = 0.2 * Math.abs(event.getX(0) - event.getX(1));
                selectedGaussian.setSD(deviation);

                // and send the result to the server
                nm.postDeviationMessage(channel, deviation);

                selectedGaussian.setAmplitude(amplitude);
                selectedGaussian.setMean(mean);

                // post the values to the server
                nm.postAmplitudeMessage(channel, amplitude);
                nm.postMeanMessage(channel, mean);
            } else if (!adjustingVariance) {
                mean = event.getX();
                amplitude = getAmplitudeFromY(event.getY());

                selectedGaussian.setAmplitude(amplitude);
                selectedGaussian.setMean(mean);

                // post the values to the server
                nm.postAmplitudeMessage(channel, amplitude);
                nm.postMeanMessage(channel, mean);
            }

        } else {
            // finish adjusting the variance if the user releases both fingers
            if (event.getPointerCount() == 1) {
                adjustingVariance = false;
                System.out.println("No fingers touching.");
            }
        }

        // update the look, since the values have changed
        draw(getHolder());

        return true;
    }

    /** Non static because the height needs to be retrieved from the
     * instance.
     * @return An amplitude; that is between -1 and 1
     */
    private double getAmplitudeFromY(double y) {
        double ratio = y / getHeight();
        double amplitude = -2 * ratio + 1;
        // clamp
        if (amplitude > 1)
            amplitude = 1;
        else if (amplitude < -1)
            amplitude = -1;

        return amplitude;
    }

    /** Gets the mean from touches.
     * Just get the average of all the touches
     */
    private static Vector2 getMeanPointFromTouches(MotionEvent e) {
        // get the average Y position of the touches
        double averageY = 0.0;
        double averageX = 0.0;
        for (int i = 0; i < e.getPointerCount(); i++) {
            averageY += e.getY(i);
            averageX += e.getX(i);
        }
        averageX = averageX / e.getPointerCount();
        averageY = averageY / e.getPointerCount();

        return new Vector2(averageX, averageY);
    }

    public void draw(SurfaceHolder holder) {
        Canvas c = holder.lockCanvas();

        // draw the app background color
        // and then draw the rounded background
        c.drawColor(APP_BACKGROUND_COLOR);
        RectF roundedBackground = new RectF(0, 0, getWidth(), getHeight());
        c.drawRoundRect(roundedBackground, ROUNDING_RADIUS,
                        ROUNDING_RADIUS, backgroundPaint);

        drawScales(c);

        // draw the actual graph
        drawGraph(c);

        holder.unlockCanvasAndPost(c);
    }

    /** Draw the overall graph shape based on the gaussians
     */
    public void drawGraph(Canvas c) {
        final double amplitude = -1.0 * GAUSSIAN_SCALAR * (getHeight() - VERT_MARGIN * 2.0);
        final double midY = getHeight() / 2.0;
        List<Vector2> curve = new ArrayList<Vector2>();

        // add the first point
        double valueAtOrigin = 0.0;
        for (AmplifiedGaussian dist : gaussians)
            valueAtOrigin += dist.getValue(0) * amplitude;
        valueAtOrigin += getHeight() / 2.0;
        curve.add(new Vector2(0,valueAtOrigin));

        // add all the middle points
        for (int i = 0; i < getWidth(); i+=2) {
            double valueAtI = 0.0;
            for (AmplifiedGaussian dist : gaussians) {
                valueAtI += dist.getValue(i) * amplitude;
            }

            valueAtI += getHeight() / 2.0;

            // put it twice, because of the way drawLines works
            // (start, end, start, end)
            curve.add(new Vector2(i,valueAtI));
            curve.add(new Vector2(i,valueAtI));
        }

        // add the last point
        double valueAtEnd = 0.0;
        for (AmplifiedGaussian dist : gaussians)
            valueAtEnd += dist.getValue(getWidth()) * amplitude;
        valueAtEnd += getHeight() / 2.0;
        curve.add(new Vector2(getWidth() ,valueAtEnd));

        // and draw the curve to the screen
        drawLinesFromPoints(c, curve);
    }

    private void drawLinesFromPoints(Canvas c, List<Vector2> points) {

        float[] arrayForCanvas = new float[2 * points.size()];
        for (int i = 0; i < points.size(); i++) {
            arrayForCanvas[2 * i] = (float) points.get(i).X;
            arrayForCanvas[2 * i + 1] = (float) points.get(i).Y;
        }

        // draw the points
        c.drawLines(arrayForCanvas, 0, arrayForCanvas.length, linePaint);
    }

    private void drawScales(Canvas c) {
        // we need to scale the log values such that 20000 becomes width
        double widthScalar = (getWidth() - LEFT_MARGIN) /
                             (Math.log(FREQ_END / 50.0));

        // draw all vertical (logarithmic scale) lines
        for (int i = 1; i < 10; i++) {
            double logValue;
            int multiplier;

            // for each multiple
            for (multiplier = 10; multiplier <= 10000; multiplier *= 10) {
                // cut out some of the values
                if (!((multiplier == 10 && i < 5) ||
                    (multiplier == 10000 && i > 2))) {

                    logValue = widthScalar * (Math.log((i * multiplier) / 50.0)) + LEFT_MARGIN;
                    c.drawLine((float) logValue, 0, (float) logValue, getHeight(), vertGridPaint);
                }
            }
        }

        // draw all the horizontal lines and the labels
        int verticalInterval = (int) Math.round((double) (getHeight() - 2 * VERT_MARGIN) / (NUM_ROWS - 1));
        int position = VERT_MARGIN;
        for (int i = 0; i < NUM_ROWS; i++) {
            // line
            c.drawLine(0, position, getWidth(), position, horizGridPaint);

            int valueInterval = (int) Math.round(Y_SCALE_AMPLITUDE / 2.0);
            String value = String.valueOf(Y_SCALE_AMPLITUDE -
                                          i * valueInterval);

            // draw the number
            drawTextWithBackground(c, value,
                    Y_SCALE_MARGIN, position + Y_SCALE_TEXT_SHIFT);


            position += verticalInterval;
        }

        // draw the scale text
        drawXScaleText(c, widthScalar);
    }

    /** Draw the numbers on X scale. This involves first drawing
     * a white rectangle of the right size then drawing the text
     */
    private void drawXScaleText(Canvas c, double widthScalar) {
        // draw the X axis text
        double logValue = widthScalar * (Math.log(100.0 / 50.0)) + LEFT_MARGIN;
        drawTextWithBackground(c, "100", logValue - TEXT_XPOS_SUBTRACTION, getHeight() - TEXT_BOTTOM_MARGIN);

        logValue = widthScalar * (Math.log(1000.0 / 50.0)) + LEFT_MARGIN;
        drawTextWithBackground(c, "1000", logValue - TEXT_XPOS_SUBTRACTION, getHeight() - TEXT_BOTTOM_MARGIN);

        logValue = widthScalar * (Math.log(10000.0 / 50.0)) + LEFT_MARGIN;
        drawTextWithBackground(c, "10k", logValue - TEXT_XPOS_SUBTRACTION, getHeight() - TEXT_BOTTOM_MARGIN);
    }

    /** Draws text, but with a background square underneath.
     * Origin is bottom left.
     * This uses the paints defined in the constructor of this class
     * hence it's non static nature. */
    private void drawTextWithBackground(Canvas c, String text, double x, double y) {
        Rect textBounds = new Rect();
        scaleTextPaint.getTextBounds(text, 0, text.length(), textBounds);
        textBounds.offset((int) Math.round(x),0);
        double textHeight = textBounds.height();
        double textWidth = textBounds.width();

        // draw the background rectangle
        c.drawRect((float) x,
                   (float) (y - textHeight - 2 * TEXT_BOX_BUFFER),
                   (float) (x + textWidth + 2 * TEXT_BOX_BUFFER),
                   (float) y,
                   scaleTextRectPaint);

        // draw the text. Remember; text is drawn with coords at the
        // bottom left
        c.drawText(text,
                   (float) x + TEXT_BOX_BUFFER,
                   (float) y,
                   scaleTextPaint);
    }

    /** used to allow different GUI elements to interface with this.
     * @param value The amplitude to set (between -1 and 1)
     * @param channel which channel to operate on (starting from 0)
     */
    public void setAmplitude(double value, int channel) {
        try {
            gaussians.get(channel).setAmplitude(value);
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Invalid channel: " + e.getMessage());
        }

        draw(holder);
    }

    /** Get the amplitude from a particular channel. This is so that
     * other gui elements can synchronize with these values.
     * @param channel The channel from which to get the amplitude.
     */
    public double getAmplitude(int channel) {
        return gaussians.get(channel).getAmplitude();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // just draw a black background when the surface
        // is first created, since the canvas will be resized
        // which is when we will actually draw the graph
        Canvas c = holder.lockCanvas();

        c.drawColor(0xff000000);

        holder.unlockCanvasAndPost(c);

        // populate the initial distributions
        // this must be done here because getWidth() returns 0
        // in the constructor
        gaussians = new ArrayList<AmplifiedGaussian>();
        for (int i = 0; i < CHANNEL_COUNT; i++) {
            double mean = (getWidth() / (double) CHANNEL_COUNT) * (double) i;
            AmplifiedGaussian distribution = new AmplifiedGaussian (
                mean, INITIAL_STANDARD_DEVIATION);

            gaussians.add(distribution);
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // do something later
        draw(holder);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // do things at the end
    }
}
