package com.app.abletonremote;

import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Runnable;

public class NetworkManager {
    
    private final int SERVER_PORT = 1235;
    private final String SERVER_IP = "192.168.1.70";

    // for the frequency view
    private final char FREQ_BIT = 'F';
    private final char AMPLITUDE_BIT = 'A';
    private final char DEVIATION_BIT = 'D';
    private final char MEAN_BIT = 'M';

    private final int RETRY_TIME = 10000;

    private Socket socket;
    private boolean connected;

    public NetworkManager() {
        connected = false;

        // start the socket opener in a new thread
        new Thread(new NetworkRunnable()).start();
    }

    // when changing the amplitude at a given channel
    public void postAmplitudeMessage(int channel, double amplitude) {
        sendMessage(FREQ_BIT + " " + AMPLITUDE_BIT + " " +
                    channel + " " + amplitude);
    }

    // when changing the standard deviation at a given channel
    public void postDeviationMessage(int channel, double deviation) {
        sendMessage(FREQ_BIT + " " + DEVIATION_BIT + " " + 
                    channel + " " + deviation);
    }

    // when changing the mean at a given channel
    public void postMeanMessage(int channel, double mean) {
        sendMessage(FREQ_BIT + " " + MEAN_BIT + " " +
                channel + " " + mean);
    }

    private void sendMessage(String message) {
        if (connected) {
            try {
                // write a test line to the socket
                PrintWriter writer = new PrintWriter(
                        socket.getOutputStream(), true);

                writer.println(message);
            } catch (IOException e) {
                System.err.println("Failed to write to socket: " +
                        e.getMessage());
            } catch (NullPointerException e) {
                System.err.println("Failed to get socket stream: " +
                        e.getMessage());
            }
        }
    }

    public void closeConnection() {
        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Couldn't close connection: " + 
                                e.getMessage());
        } catch (NullPointerException e) {
            System.err.println("Connection hasn't started; can't close.");
        }
    }

    private class NetworkRunnable implements Runnable {
        public void run() {
            while (!connected) {
                try {
                    // open the socket to the server
                    System.out.println("Attempting to create server...");
                    socket = new Socket(SERVER_IP, SERVER_PORT);
                    System.out.println("Successful.");
                    connected = true;
                } catch (UnknownHostException e) {
                    System.err.println("Hostname could not be resolved: " +
                            e.getMessage());
                } catch (IOException e) {
                    System.err.println("Could not create socket: " +
                            e.getMessage());
                }

                try {
                    Thread.sleep(RETRY_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
